from flask import  Flask, flash, request, redirect, url_for
from flask import render_template
from flask import request
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
from flask import send_file
import random
import os
import sqlite3

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif', 'mp3', 'mp4', 'wav', 'flac', 'm4a'])
app = Flask(__name__)
UPLOAD_FOLDER = os.path.join('app/static/lol')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

'''
todo list

only allow upload of pix

more lolsorandomxd css
-when adding lots of tags, make it so only ~5 "crazy" tags are chosen at once

better screen support, tons of pics, just make it big random

only 3 mp3s play at once

diffrent years diffeent pages

make automatically change

'''

S3_BUCKET                 = os.environ.get("S3_BUCKET")
S3_KEY                    = os.environ.get("S3_KEY")
S3_SECRET                 = os.environ.get("S3_SECRET_ACCESS_KEY")
S3_LOCATION               = 'http://{}.s3.amazonaws.com/'.format(S3_BUCKET)
SECRET_KEY                = os.urandom(32)

import boto3, botocore

s3 = boto3.client(
   "s3",
   aws_access_key_id=S3_KEY,
   aws_secret_access_key=S3_SECRET
)

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/',  methods=['GET', 'POST'])
def index():

  if request.method == 'POST':
    if 'file' not in request.files:
      # flash('No file part')
      return redirect(request.url)
    file = request.files['file']
    # # if user does not select file, browser also
    # # submit an empty part without filename
    if file.filename == '':
        # flash('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename): 
      filename = secure_filename(file.filename)
      # This line seeems to corrupt the s3 uploads
      # file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
      upload_file_to_s3(file, S3_BUCKET)
      final_file_arr = get_file_obj('s3')

      return render_template("index.html", file_arr=final_file_arr, msg='thanks for ur pic')
  elif request.method == 'GET':
    final_file_arr = get_file_obj('s3')
    return render_template("index.html", file_arr=final_file_arr, msg='')



def upload_file_to_s3(file, bucket_name, acl="public-read"):
  file_name = file.filename
  s3.upload_fileobj(
    file,
    'mumblepicrandom',
    file.filename
  )

def get_file_obj(source):
  if source == 'local':
    images = os.listdir(os.path.join(app.static_folder, "lol"))
  else:
    files = get_all_files_from_s3()
  un_randomed_file_arr = []
  for s3_file_url in files:
    file_obj = {}
    file_obj['file'] = s3_file_url

    file_obj['type'] = figure_out_file_type(s3_file_url)

    file_obj['ext'] = get_file_ext(s3_file_url)

    un_randomed_file_arr.append(file_obj)

  
  img_arr = []
  aud_arr = []
  for file_obj in un_randomed_file_arr:
    if file_obj['type'] == 'img':
      img_arr.append(file_obj)
    elif file_obj['type'] == 'audio':
      aud_arr.append(file_obj)

  img_arr_len = len(img_arr)
  forty_percent_of_img_arr = float(img_arr_len) * .4

  aud_arr_len = len(aud_arr)
  forty_percent_of_aud_arr = float(aud_arr_len) * .4

  random_img_arr = []
  for times_to_find_a_random_number in range(int(forty_percent_of_img_arr)):
    rand_num = random.randint(0, img_arr_len)
    img_arr_len = img_arr_len - 1
    random_img_arr.append(rand_num)

  random_aud_arr = []
  for times_to_find_a_random_number in range(int(forty_percent_of_aud_arr)):
    rand_num = random.randint(0, aud_arr_len)
    aud_arr_len = aud_arr_len - 1
    random_aud_arr.append(rand_num)

  for index_to_remove in random_img_arr:
    try:
      img_arr.remove(img_arr[index_to_remove])
    except IndexError:
      print('no img at index at', index_to_remove)
  
  for index_to_remove in random_aud_arr:
    try:
      aud_arr.remove(aud_arr[index_to_remove])
    except IndexError:
      print('no img at index at', index_to_remove)
  
  final_file_arr = aud_arr + img_arr
  for file in final_file_arr:
    rand_width_px = random.randint(100, 500)
    rand_height_px = random.randint(100, 500)
    rand_transform_deg = random.randint(-360, 360)
    rand_bottom_px = random.randint(50, 1500)
    rand_right_px = random.randint(50, 2000)
    rand_z_index = random.randint(-5000, 7000)

    file['rand_css'] = 'width:%spx; height:%spx; transform:rotate(%sdeg); bottom:%spx; right:%spx; z-index:%s; position:absolute;' % (rand_width_px, rand_height_px, rand_transform_deg, rand_bottom_px, rand_right_px, rand_z_index)

  return final_file_arr

def figure_out_file_type(s3_file_url):
  file_extension = get_file_ext(s3_file_url)
  if file_extension == 'mp3' or file_extension == 'mp4' or file_extension == 'wav' or file_extension == 'flac' or file_extension == 'm4a':
    return 'audio'
  elif file_extension == 'png' or file_extension == 'jpg' or file_extension == 'jpeg' or file_extension == 'gif':
    return 'img'
  else:
    return 'unknown'

def get_file_ext(s3_file_url):
  return s3_file_url.rsplit('.', 1)[1].lower()

def get_all_files_from_s3():
  all_objects = s3.list_objects(Bucket = 'mumblepicrandom') 
  all_s3_files = []
  for file in all_objects['Contents']:
    all_s3_files.append(
      S3_LOCATION + file['Key']
    )
  return all_s3_files

if __name__ == '__main__':
  app.run(debug=True)